import ExpenseItem from './ExpenseItem'
import './Expenses.css'

function Expenses(props) {
	const data = props.items;
	return(
      <div className="expenses">
         {
            data.map(el => {
               return (
                  <ExpenseItem 
                  key={el.id}
                  title={el.title}
                  date={el.date}
                  amount={el.amount}
                   />
               )
            })
         }
      </div>
      ) 

	// <div>
	// 	{props.map((el) => {
	//       return (
	//          <ExpenseItem title={el.title} date={el.date} amount={el.amount}/>
	//       )
	// 	})}
	// </div>
}

export default Expenses
